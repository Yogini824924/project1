// ==== Problem #2 ====
// The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory is?  Log the make and model into the console in the format of: 
//"Last car is a *car make goes here* *car model goes here*"


let function2 = function(arr) {
	if(!(Array.isArray(arr)) || arr.length == 0) {
//		console.log(Array.isArray(function2.arguments[0]));
		return [];
	}
	else {
		return arr[arr.length-1];
	}
}

module.exports = function2;
