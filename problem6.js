// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  
//Execute a function and return an array that only contains BMW and Audi cars.
//Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.

let function6 = function(arr) {
	if(!(Array.isArray(arr))) {
		return [];
	}
	let BMWAndAudi=[];
	for( let i = 0; i < arr.length; i++ ) {
		if( arr[i]["car_make"] === "BMW" || arr[i]["car_make"] === "Audi" ) {
			BMWAndAudi.push(arr[i]);
		}
	}
	return BMWAndAudi;
}

module.exports = function6;
