// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot.
// Execute a function that will return an array from the dealer data containing only the car years and 
//log the result in the console as it was returned.


let function4 = function(arr) {
	if(!(Array.isArray(arr))) {
		return [];
	}
	let years=[];
	for( let i = 0; i < arr.length; i++ ) {
		years.push(arr[i]["car_year"]);
	}
	return years;
}

module.exports = function4;
