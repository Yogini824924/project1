// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website.
// Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.

let function3 = function(arr) {
	if(!(Array.isArray(arr))) {
		return [];
	}
	for( let i = 0; i < arr.length-1; i++) { 
		for( let j = i+1; j < arr.length; j++) {
			if( arr[i]["car_model"] > arr[j]["car_model"] ) {
				let q = arr[i];
				arr[i] = arr[j];
				arr[j] = q;
			}
		}
	}
	return arr;
}

module.exports = function3;
