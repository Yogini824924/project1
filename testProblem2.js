const problem2 = require("./problem2.js");
const inventory = require("./inventory.js");

let obj = problem2(inventory);
if(Array.isArray(obj)) {
	console.log(obj);
}
else {
	console.log(`Last car is a ${obj["car_make"]} ${obj["car_model"]}`);
}
